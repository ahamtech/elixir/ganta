defmodule Ganta do
  @moduledoc """
  Documentation for `Ganta`.

  Module to Push Alerts Directly to Gitlab.

  reference: https://gitlab.com/help/user/project/integrations/generic_alerts.md#setting-up-generic-alerts
  """
  @type request_payload :: %{
          title: String.t(),
          description: String.t(),
          start_time: String.t(),
          service: String.t(),
          monitoring_tool: String.t(),
          hosts: [String.t()],
          severity: Sting.t(),
          fingerprint: String.t(),
          payload: term(),
          endpoint: String.t(),
          auth_key: String.t()
        }

  @doc """
  sample_payload = %{
  title: "Ganta Title",
  description: "Runtime Error",
  start_time: "2019-07-10",
  monitoring_tool: "Ganta",
  hosts: "127.0.0.1",
  severity: "Critical",
  fingerprint: sha256,
  payload: %{
    sample: "help"
    }
  }
  """
  @spec validate_request(request_payload()) :: {:ok, request_payload()}
  def validate_request(params) do
    scheme =
      Optimal.schema(
        opts: [
          title: :string,
          description: :string,
          endpoint: :string,
          start_time: :string,
          monitoring_tool: :string,
          service: :string,
          hosts: {:list, :string},
          severity: :string,
          fingerprint: :string,
          auth_key: :string,
          payload: :any
        ],
        required: [:title, :endpoint, :auth_key],
        defaults: [
          severity: "unknown",
          endpoint: Application.get_env(:ganta, :endpoint),
          auth_key: Application.get_env(:ganta, :auth_key)
        ]
      )

    case Optimal.validate(params, scheme) do
      {:ok, payload} ->
        {:ok, payload}

      {:error, error} ->
        {:error, error}
    end
  end

  @spec send_request(request_payload()) :: %HTTPoison.Response{} 
  def send_request(payload) do
    auth_key = payload[:auth_key]
    url = payload[:endpoint]
    payload = payload |> Map.new() |> Map.drop([:endpoint, :auth_key]) |> Jason.encode!()
    headers = [{"Authorization", "Bearer #{auth_key}"}, {"Content-Type", "application/json"}]
    HTTPoison.post(url, payload, headers)
  end
end
